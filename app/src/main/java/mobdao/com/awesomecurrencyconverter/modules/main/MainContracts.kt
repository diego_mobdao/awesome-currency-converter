package mobdao.com.awesomecurrencyconverter.modules.main

import mobdao.com.awesomecurrencyconverter.models.entities.ConvertedCurrency
import mobdao.com.awesomecurrencyconverter.modules.base.BaseContracts
import mobdao.com.awesomecurrencyconverter.utils.enums.CurrencyType

interface MainContracts {

    interface View : BaseContracts.View {
        fun updateAllCurrencies(
            convertedCurrencies: List<ConvertedCurrency>?
        )

        fun updateAllCurrenciesButBase(
            convertedCurrencies: List<ConvertedCurrency>?
        )

        fun updateCurrencyPosition(
            fromPosition: Int,
            toPosition: Int,
            currencies: List<ConvertedCurrency>
        )
    }

    interface Presenter : BaseContracts.Presenter {
        fun onCurrencyClicked(
            currencyType: CurrencyType
        )

        fun onBaseCurrencyValueChanged(
            value: Double
        )
    }
}