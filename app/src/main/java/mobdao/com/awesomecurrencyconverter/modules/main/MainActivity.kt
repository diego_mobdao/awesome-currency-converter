package mobdao.com.awesomecurrencyconverter.modules.main

import android.os.Bundle
import kotlinx.android.synthetic.main.activity_main.*
import mobdao.com.awesomecurrencyconverter.R
import mobdao.com.awesomecurrencyconverter.models.entities.ConvertedCurrency
import mobdao.com.awesomecurrencyconverter.modules.base.BaseActivity
import mobdao.com.awesomecurrencyconverter.ui_components.adapters.CurrencyAdapter
import mobdao.com.awesomecurrencyconverter.utils.extensions.dismissKeyboardOnScroll
import mobdao.com.awesomecurrencyconverter.utils.extensions.removeBlinkAnimationOnUpdate
import mobdao.com.awesomecurrencyconverter.utils.extensions.scrollToTop

class MainActivity :
    BaseActivity<MainPresenter>(R.id.rootLayout, R.id.progressBar),
    MainContracts.View {

    private var adapter: CurrencyAdapter? = null

    override fun instantiatePresenter(): MainPresenter {
        return MainPresenter(this)
    }

    //region lifecycle

    override fun onCreate(
        savedInstanceState: Bundle?
    ) {
        super.onCreate(savedInstanceState)
        setContentView(R.layout.activity_main)
        lifecycle.addObserver(presenter)
        setupView()
    }

    //endregion

    //region View

    override fun updateAllCurrencies(
        convertedCurrencies: List<ConvertedCurrency>?
    ) {
        adapter?.loadCurrencies(convertedCurrencies.orEmpty())
    }

    override fun updateAllCurrenciesButBase(
        convertedCurrencies: List<ConvertedCurrency>?
    ) {
        adapter?.updateAllCurrenciesButBase(convertedCurrencies.orEmpty())
    }

    override fun updateCurrencyPosition(
        fromPosition: Int,
        toPosition: Int,
        currencies: List<ConvertedCurrency>
    ) {
        adapter?.moveCurrencyToTop(fromPosition, toPosition, currencies)
        recyclerView.scrollToTop()
    }

    //endregion

    //region private

    private fun setupView() {
        setSupportActionBar(toolbar)
        setupRecyclerView()
    }

    private fun setupRecyclerView() {
        CurrencyAdapter(
            itemClickListener = { clickedCurrencyType ->
                toolbar.isFocusable = false
                presenter.onCurrencyClicked(clickedCurrencyType)
            },
            baseValueListener = { updatedBaseValue ->
                presenter.onBaseCurrencyValueChanged(updatedBaseValue)
            }
        ).run {
            adapter = this
            recyclerView.adapter = adapter
        }

        recyclerView.dismissKeyboardOnScroll()
        recyclerView.removeBlinkAnimationOnUpdate()
    }

    //endregion
}
