package mobdao.com.awesomecurrencyconverter.modules.splash

import android.content.Intent
import android.os.Bundle
import androidx.appcompat.app.AppCompatActivity
import mobdao.com.awesomecurrencyconverter.modules.main.MainActivity

class SplashActivity : AppCompatActivity() {

    //region Lifecycle

    override fun onCreate(
        savedInstanceState: Bundle?
    ) {
        super.onCreate(savedInstanceState)
        startMainModule()
    }

    //endregion

    //region private

    private fun startMainModule() {
        startActivity(
            Intent(this, MainActivity::class.java)
        )
        finish()
    }

    //endregion

}
