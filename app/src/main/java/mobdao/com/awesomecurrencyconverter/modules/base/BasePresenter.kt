package mobdao.com.awesomecurrencyconverter.modules.base

import androidx.lifecycle.Lifecycle
import androidx.lifecycle.LifecycleObserver
import androidx.lifecycle.OnLifecycleEvent
import io.reactivex.disposables.CompositeDisposable
import mobdao.com.awesomecurrencyconverter.di.modules.ContextModule
import mobdao.com.awesomecurrencyconverter.di.components.DaggerPresenterComponent
import mobdao.com.awesomecurrencyconverter.di.components.PresenterComponent
import mobdao.com.awesomecurrencyconverter.di.modules.RepositoryModule
import mobdao.com.awesomecurrencyconverter.modules.main.MainPresenter

abstract class BasePresenter<out V : BaseContracts.View>(
    protected val view: V
) : BaseContracts.Presenter,
    LifecycleObserver {

    protected val compositeDisposable = CompositeDisposable()

    private val injector: PresenterComponent = DaggerPresenterComponent
        .builder()
        .baseView(view)
        .contextModule(ContextModule)
        .repositoryModule(RepositoryModule)
        .build()

    init {
        inject()
    }

    //region lifecycle

    @OnLifecycleEvent(Lifecycle.Event.ON_DESTROY)
    open fun onDestroy() {
        compositeDisposable.clear()
    }

    //endregion

    //region private

    private fun inject() {
        when (this) {
            is MainPresenter -> injector.inject(this)
        }
    }

    //endregion
}