package mobdao.com.awesomecurrencyconverter.modules.main

import android.annotation.SuppressLint
import android.app.Application
import androidx.lifecycle.Lifecycle
import androidx.lifecycle.LifecycleObserver
import androidx.lifecycle.OnLifecycleEvent
import io.reactivex.android.schedulers.AndroidSchedulers
import io.reactivex.disposables.Disposable
import io.reactivex.rxkotlin.subscribeBy
import io.reactivex.schedulers.Schedulers
import mobdao.com.architecturesample.utils.helpers.NetworkHelper
import mobdao.com.awesomecurrencyconverter.R
import mobdao.com.awesomecurrencyconverter.data.repositories.CurrencyRepository
import mobdao.com.awesomecurrencyconverter.models.CurrencyRate
import mobdao.com.awesomecurrencyconverter.models.entities.ConvertedCurrency
import mobdao.com.awesomecurrencyconverter.modules.base.BasePresenter
import mobdao.com.awesomecurrencyconverter.utils.enums.CurrencyType
import mobdao.com.awesomecurrencyconverter.utils.extensions.orZero
import java.util.*
import java.util.concurrent.TimeUnit
import javax.inject.Inject

class MainPresenter(
    mainView: MainContracts.View
) : BasePresenter<MainContracts.View>(mainView),
    MainContracts.Presenter,
    LifecycleObserver {

    @Inject
    lateinit var application: Application

    @Inject
    lateinit var currencyRepository: CurrencyRepository

    private var dbCurrenciesDisposable: Disposable? = null
    private var apiSubscriptionDelay = 1L // second
    private val isDataBaseBusy: Boolean
        get() = currencyRepository.isInTransaction()

    private var convertedCurrencies = arrayListOf<ConvertedCurrency>()

    // region lifecycle

    @OnLifecycleEvent(Lifecycle.Event.ON_CREATE)
    fun onCreate() {
        loadSavedCurrencies()
    }

    @OnLifecycleEvent(Lifecycle.Event.ON_RESUME)
    fun onResume() {
        fetchComparedRates()
    }

    @OnLifecycleEvent(Lifecycle.Event.ON_STOP)
    fun onStop() {
        compositeDisposable.clear()
    }

    override fun onDestroy() {
        super.onDestroy()
        saveCurrencies()
    }

    //endregion

    //region Presenter

    override fun onCurrencyClicked(
        currencyType: CurrencyType
    ) {
        if (getBaseCurrencyType() != currencyType) {
            setAsBaseCurrency(currencyType)
            compositeDisposable.clear()
            fetchComparedRates()
        }
    }

    override fun onBaseCurrencyValueChanged(
        value: Double
    ) {
        updateBaseTextValue(value)
        view.updateAllCurrenciesButBase(convertedCurrencies)
    }

    override fun onRetryClicked() {
        fetchComparedRates()
    }

    //endregion

    //region private

    private fun onNewRatesFetched(
        newCurrencyRates: List<CurrencyRate>
    ) {
        val previousBase = getBaseCurrency()
        addOrUpdateCurrencyRates(newCurrencyRates)
        applyBaseValueToRates(getBaseCurrency()?.convertedValue.orZero())

        if (previousBase?.type == getBaseCurrency()?.type) {
            view.updateAllCurrenciesButBase(convertedCurrencies)
        } else {
            view.updateAllCurrencies(convertedCurrencies)
        }
    }

    private fun addOrUpdateCurrencyRates(
        newCurrencyRates: List<CurrencyRate>
    ) {
        if (convertedCurrencies.isEmpty()) {
            // add base first
            convertedCurrencies.add(
                ConvertedCurrency(
                    type = getBaseCurrencyType(),
                    rate = 1.0,
                    isBase = true,
                    convertedValue = 100.0
                )
            )
        }

        newCurrencyRates.forEach { newCurrencyRate ->
            val convertedCurrency = convertedCurrencies.firstOrNull { convertedCurrency ->
                newCurrencyRate.code == convertedCurrency.type.code && !convertedCurrency.isBase
            }
            if (convertedCurrency == null) {
                CurrencyType.from(newCurrencyRate.code)?.let { type ->
                    convertedCurrencies.add(
                        ConvertedCurrency(
                            type = type,
                            rate = newCurrencyRate.rate,
                            isBase = false,
                            convertedValue = newCurrencyRate.rate
                        )
                    )
                }
            } else {
                convertedCurrency.rate = newCurrencyRate.rate
            }
        }
    }

    private fun setAsBaseCurrency(
        currencyType: CurrencyType
    ) {
        val index = convertedCurrencies.indexOfFirst { it.type == currencyType }
        if (index <= 0) return

        convertedCurrencies.forEach { it.isBase = false }

        val newBase = convertedCurrencies.removeAt(index)
        newBase.isBase = true

        convertedCurrencies.add(0, newBase)
        updateRatesWithNewBase(newBase.rate)

        view.updateCurrencyPosition(index, 0, convertedCurrencies)
    }

    private fun updateRatesWithNewBase(
        oldRateOfNewBase: Double
    ) {
        convertedCurrencies.forEach { convertedCurrency ->
            convertedCurrency.rate = convertedCurrency.rate / oldRateOfNewBase
        }
    }

    private fun updateBaseTextValue(
        baseValue: Double
    ) {
        getBaseCurrency()?.convertedValue = baseValue
        applyBaseValueToRates(baseValue)
    }

    private fun applyBaseValueToRates(
        baseValue: Double
    ) {
        convertedCurrencies.forEach { convertedCurrency ->
            convertedCurrency.convertedValue = baseValue * convertedCurrency.rate
        }
    }

    private fun getBaseCurrency(): ConvertedCurrency? =
        convertedCurrencies.firstOrNull { it.isBase }

    private fun getBaseCurrencyType(): CurrencyType =
        getBaseCurrency()?.type
            ?: CurrencyType.from(Currency.getInstance(Locale.getDefault()).currencyCode)
            ?: CurrencyType.EUR

    //endregion

    //region repository

    private fun loadSavedCurrencies() {
        view.showProgressDialog()
        dbCurrenciesDisposable = currencyRepository.getSaveConvertedCurrencies()
            .subscribeOn(Schedulers.io())
            .observeOn(AndroidSchedulers.mainThread())
            .skipWhile { isDataBaseBusy } // DB might still be saving from onDestroy() call
            .subscribeBy(
                onNext = { convertedCurrencies ->
                    if (convertedCurrencies.isNotEmpty()) {
                        this.convertedCurrencies = ArrayList(convertedCurrencies)
                        view.updateAllCurrencies(convertedCurrencies)
                        view.hideProgressDialog()
                    }
                    onFinishedLoadingFromDB()
                },
                onError = { throwable ->
                    onFinishedLoadingFromDB()
                    throwable.printStackTrace()
                }
            )
        dbCurrenciesDisposable?.let { compositeDisposable.add(it) }
    }

    private fun onFinishedLoadingFromDB() {
        dbCurrenciesDisposable?.let { compositeDisposable.remove(it) }
        fetchComparedRates()
    }

    @SuppressLint("CheckResult")
    private fun saveCurrencies() {
        if (convertedCurrencies.isEmpty()) return

        // No need to dispose as it is called when view is being destroyed
        currencyRepository.saveConvertedCurrencies(convertedCurrencies)
            .subscribeOn(Schedulers.io())
            .observeOn(AndroidSchedulers.mainThread())
            .subscribeBy(
                onError = { throwable ->
                    throwable.printStackTrace()
                })
    }

    private fun fetchComparedRates() {
        // wait to finish loading from db first
        if (dbCurrenciesDisposable?.isDisposed != true) return

        view.showProgressDialog()
        compositeDisposable.add(currencyRepository.fetchComparedRates(getBaseCurrencyType().code)
            .repeatWhen { flowable -> flowable.delay(apiSubscriptionDelay, TimeUnit.SECONDS) }
            .subscribeOn(Schedulers.io())
            .observeOn(AndroidSchedulers.mainThread())
            .subscribeBy(
                onNext = { convertedCurrencies ->
                    view.hideError()
                    onNewRatesFetched(convertedCurrencies)
                    view.hideProgressDialog()
                },
                onError = {
                    view.hideProgressDialog()
                    if (!NetworkHelper.isConnected(application)) {
                        view.showInternetError(enqueue = true)
                    } else {
                        view.showError(R.string.fetch_currencies_error_message)
                    }
                }
            )
        )
    }

    //endregion
}