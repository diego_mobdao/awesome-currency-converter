package mobdao.com.awesomecurrencyconverter.modules.base

import android.annotation.SuppressLint
import android.content.Context
import android.os.Bundle
import androidx.annotation.IdRes
import androidx.annotation.StringRes
import androidx.appcompat.app.AppCompatActivity
import androidx.core.widget.ContentLoadingProgressBar
import com.google.android.material.snackbar.Snackbar
import mobdao.com.awesomecurrencyconverter.R

@SuppressLint("Registered")
abstract class BaseActivity<P : BasePresenter<BaseContracts.View>>(
    @IdRes private val rootLayoutId: Int? = null,
    @IdRes private val progressDialogId: Int? = null

) : AppCompatActivity(), BaseContracts.View {

    protected lateinit var presenter: P
    private var snackbar: Snackbar? = null
    private val progressDialog: ContentLoadingProgressBar? by lazy {
        progressDialogId?.let { findViewById<ContentLoadingProgressBar>(it) }
    }

    override fun onCreate(savedInstanceState: Bundle?) {
        super.onCreate(savedInstanceState)
        presenter = instantiatePresenter()
    }

    override fun getContext(): Context {
        return this
    }

    override fun showError(
        @StringRes messageRes: Int,
        enqueue: Boolean
    ) {
        if (rootLayoutId == null || (!enqueue && snackbar?.isShown == true)) return

        snackbar = Snackbar.make(
            findViewById(rootLayoutId),
            messageRes,
            Snackbar.LENGTH_INDEFINITE
        )
        snackbar?.setAction(R.string.retry) {
            presenter.onRetryClicked()
        }
        snackbar?.show()
    }

    override fun showInternetError(enqueue: Boolean) {
        showError(R.string.internet_error_message, enqueue)
    }

    override fun hideError() {
        snackbar?.dismiss()
    }

    override fun showProgressDialog() {
        progressDialog?.show()
    }

    override fun hideProgressDialog() {
        progressDialog?.hide()
    }

    protected abstract fun instantiatePresenter(): P
}