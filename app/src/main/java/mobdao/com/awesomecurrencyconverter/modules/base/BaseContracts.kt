package mobdao.com.awesomecurrencyconverter.modules.base

import android.content.Context
import androidx.annotation.StringRes

interface BaseContracts {

    interface View {
        fun getContext(): Context
        fun showError(
            @StringRes messageRes: Int,
            enqueue: Boolean = false
        )

        fun showInternetError(enqueue: Boolean = false)
        fun hideError()
        fun showProgressDialog()
        fun hideProgressDialog()
    }

    interface Presenter {
        fun onRetryClicked() {}
    }
}