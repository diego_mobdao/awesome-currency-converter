package mobdao.com.awesomecurrencyconverter.utils.extensions

fun Int?.orZero(): Int = this ?: 0