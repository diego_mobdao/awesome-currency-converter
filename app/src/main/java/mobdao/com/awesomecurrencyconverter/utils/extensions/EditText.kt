package mobdao.com.awesomecurrencyconverter.utils.extensions

import android.text.Editable
import android.text.InputFilter
import android.text.InputType
import android.text.TextWatcher
import android.widget.EditText

fun EditText.afterTextChanged(
    afterTextChanged: (String?) -> Unit
) {
    addTextChangedListener(object : TextWatcher {
        override fun afterTextChanged(text: Editable?) {
            text?.toString().let(afterTextChanged)
        }

        override fun beforeTextChanged(p0: CharSequence?, p1: Int, p2: Int, p3: Int) {}
        override fun onTextChanged(p0: CharSequence?, p1: Int, p2: Int, p3: Int) {}
    })
}

fun EditText.keepEditTwoPoint() {
    inputType = InputType.TYPE_NUMBER_FLAG_DECIMAL or InputType.TYPE_CLASS_NUMBER
    filters = arrayOf(
        InputFilter { source, _, _, dest, _, _ ->
            if (source == "." && dest.toString().isEmpty()) {
                return@InputFilter "0."
            }
            if (dest.toString().contains(".")) {
                val index = dest.toString().indexOf(".")
                val length = dest.toString().substring(index).length
                if (length == 3) {
                    return@InputFilter ""
                }
            }
            null
        }
    )
}