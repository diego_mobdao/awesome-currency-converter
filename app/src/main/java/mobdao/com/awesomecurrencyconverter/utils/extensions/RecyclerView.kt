package mobdao.com.awesomecurrencyconverter.utils.extensions

import androidx.recyclerview.widget.RecyclerView
import androidx.recyclerview.widget.SimpleItemAnimator

fun RecyclerView.scrollToTop() {
    scrollToPosition(0)
}

fun RecyclerView.removeBlinkAnimationOnUpdate() {
    (itemAnimator as? SimpleItemAnimator)?.supportsChangeAnimations = false
}