package mobdao.com.awesomecurrencyconverter.utils.extensions

import android.view.LayoutInflater
import android.view.View
import android.view.ViewGroup
import androidx.annotation.LayoutRes

fun ViewGroup?.inflate(
    @LayoutRes res: Int,
    attachToRoot: Boolean = false
): View =
    LayoutInflater.from(this?.context).inflate(res, this, attachToRoot)