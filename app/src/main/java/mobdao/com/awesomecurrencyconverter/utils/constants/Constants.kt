package mobdao.com.awesomecurrencyconverter.utils.constants

object Constants {
    const val baseEndpointUrl = "https://revolut.duckdns.org/"
    const val appDataBaseName = "data_base"
}