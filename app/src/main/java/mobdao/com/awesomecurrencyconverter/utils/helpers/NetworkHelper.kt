@file:Suppress("DEPRECATION")

package mobdao.com.architecturesample.utils.helpers

import android.app.Application
import android.content.Context
import android.net.ConnectivityManager

object NetworkHelper {

    fun isConnected(application: Application): Boolean {
        val connectivityManager = application
            .getSystemService(Context.CONNECTIVITY_SERVICE) as? ConnectivityManager
        val activeNetwork = connectivityManager?.activeNetworkInfo
        return activeNetwork?.isConnectedOrConnecting == true
    }
}