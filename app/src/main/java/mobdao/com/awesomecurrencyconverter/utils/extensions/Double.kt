package mobdao.com.awesomecurrencyconverter.utils.extensions

import java.text.DecimalFormat

fun Double?.orZero(): Double =
    this ?: 0.0

fun Double.toCurrencyText(): String =
    DecimalFormat("#.##").format(this)