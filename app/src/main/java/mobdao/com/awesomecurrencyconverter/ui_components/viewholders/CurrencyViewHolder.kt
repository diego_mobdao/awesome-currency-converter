package mobdao.com.awesomecurrencyconverter.ui_components.viewholders

import android.view.View
import android.view.ViewGroup
import android.widget.EditText
import android.widget.ImageView
import android.widget.TextView
import androidx.recyclerview.widget.RecyclerView
import mobdao.com.awesomecurrencyconverter.R
import mobdao.com.awesomecurrencyconverter.models.entities.ConvertedCurrency
import mobdao.com.awesomecurrencyconverter.utils.enums.CurrencyType
import mobdao.com.awesomecurrencyconverter.utils.extensions.*

class CurrencyViewHolder(
    parent: ViewGroup,
    private val itemClickListener: (CurrencyType) -> Unit,
    private val baseValueListener: (Double) -> Unit
) : RecyclerView.ViewHolder(parent.inflate(R.layout.item_currency)) {

    private var convertedCurrency: ConvertedCurrency? = null
    private var notifyValueChanged = false

    // Kotlin's synthetic extension isn't appropriate for view holders
    private val currencyImageView: ImageView by lazy {
        itemView.findViewById<ImageView>(R.id.currencyImageView)
    }
    private val currencyCodeTextView: TextView by lazy {
        itemView.findViewById<TextView>(R.id.currencyCodeTextView)
    }
    private val currencyNameTextView: TextView by lazy {
        itemView.findViewById<TextView>(R.id.currencyNameTextView)
    }
    private val convertedValueEditText by lazy {
        itemView.findViewById<EditText>(R.id.convertedValueEditText)
    }
    private val clickableAreaView by lazy {
        itemView.findViewById<View>(R.id.clickableAreaView)
    }

    init {
        setupListeners()
        setupEditTexts()
    }

    fun bind(
        convertedCurrency: ConvertedCurrency
    ) {
        this.convertedCurrency = convertedCurrency
        convertedCurrency.type.run {
            currencyImageView.setImageResource(flagRes)
            currencyCodeTextView.text = code
            currencyNameTextView.text = itemView.context.getString(nameRes)
        }

        var convertedValueText = convertedCurrency.convertedValue.toCurrencyText()
        if (convertedValueText == "0") {
            convertedValueText = ""
        }

        notifyValueChanged = false
        convertedValueEditText.setText(convertedValueText)
        convertedValueEditText.setSelection(convertedValueText.length.orZero())
    }

    //region private

    private fun setupListeners() {
        clickableAreaView.setOnClickListener {
            convertedCurrency?.type?.let {
                itemClickListener.invoke(it)
                convertedValueEditText.showKeyboard()
            }
        }

        convertedValueEditText.afterTextChanged { text ->
            if (!notifyValueChanged) {
                notifyValueChanged = true
                return@afterTextChanged
            }
            text?.toDoubleOrNull().orZero().let(baseValueListener)
        }
    }

    private fun setupEditTexts() {
        convertedValueEditText.keepEditTwoPoint()
    }

    //endregion
}