package mobdao.com.awesomecurrencyconverter.ui_components.adapters

import android.view.ViewGroup
import androidx.recyclerview.widget.RecyclerView
import mobdao.com.awesomecurrencyconverter.models.entities.ConvertedCurrency
import mobdao.com.awesomecurrencyconverter.ui_components.viewholders.CurrencyViewHolder
import mobdao.com.awesomecurrencyconverter.utils.enums.CurrencyType

class CurrencyAdapter(
    private val itemClickListener: (CurrencyType) -> Unit,
    private val baseValueListener: (Double) -> Unit
) : RecyclerView.Adapter<CurrencyViewHolder>() {

    private var currencies = listOf<ConvertedCurrency>()

    override fun onCreateViewHolder(
        parent: ViewGroup,
        viewType: Int
    ): CurrencyViewHolder =
        CurrencyViewHolder(parent, itemClickListener, baseValueListener)

    override fun getItemCount(): Int =
        currencies.size

    override fun onBindViewHolder(
        holder: CurrencyViewHolder,
        position: Int
    ) {
        currencies.getOrNull(position)?.run {
            holder.bind(this)
        }
    }

    fun loadCurrencies(
        currencies: List<ConvertedCurrency>
    ) {
        this.currencies = currencies
        notifyDataSetChanged()
    }

    fun updateAllCurrenciesButBase(
        currencies: List<ConvertedCurrency>
    ) {
        this.currencies = currencies
        if (currencies.size > 1) {
            notifyItemRangeChanged(1, currencies.size - 1)
        }
    }

    fun moveCurrencyToTop(
        fromPosition: Int,
        toPosition: Int,
        currencies: List<ConvertedCurrency>
    ) {
        this.currencies = currencies
        notifyItemMoved(fromPosition, toPosition)
    }
}