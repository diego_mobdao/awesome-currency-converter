package mobdao.com.awesomecurrencyconverter.di.modules

import android.app.Application
import dagger.Module
import dagger.Provides
import dagger.Reusable
import mobdao.com.awesomecurrencyconverter.data.db.AppDatabase
import mobdao.com.awesomecurrencyconverter.data.db.daos.ConvertedCurrencyDao
import mobdao.com.awesomecurrencyconverter.data.repositories.CurrencyRepository

@Module
@Suppress("unused")
object RepositoryModule {

    @Provides
    @Reusable
    internal fun provideCurrencyRepository(
        appDatabase: AppDatabase,
        convertedCurrencyDao: ConvertedCurrencyDao
    ): CurrencyRepository {
        return CurrencyRepository(appDatabase, convertedCurrencyDao)
    }

    @Provides
    @Reusable
    internal fun provideConvertedCurrencyDao(
        appDatabase: AppDatabase
    ): ConvertedCurrencyDao {
        return appDatabase.convertedCurrencyDao()
    }

    @Provides
    @Reusable
    internal fun provideAppDataBase(
        application: Application
    ): AppDatabase {
        return AppDatabase.getInstance(application)
    }
}