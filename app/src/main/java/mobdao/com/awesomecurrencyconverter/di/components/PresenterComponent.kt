package mobdao.com.awesomecurrencyconverter.di.components

import dagger.BindsInstance
import dagger.Component
import mobdao.com.awesomecurrencyconverter.di.modules.ContextModule
import mobdao.com.awesomecurrencyconverter.di.modules.RepositoryModule
import mobdao.com.awesomecurrencyconverter.modules.base.BaseContracts
import mobdao.com.awesomecurrencyconverter.modules.main.MainPresenter
import javax.inject.Singleton

@Singleton
@Component(
    modules = [
        (ContextModule::class),
        (RepositoryModule::class)
    ]
)
interface PresenterComponent {
    fun inject(postPresenter: MainPresenter)

    @Component.Builder
    interface Builder {
        fun build(): PresenterComponent

        fun contextModule(contextModule: ContextModule): Builder
        fun repositoryModule(repositoryModule: RepositoryModule): Builder

        @BindsInstance
        fun baseView(baseView: BaseContracts.View): Builder
    }
}