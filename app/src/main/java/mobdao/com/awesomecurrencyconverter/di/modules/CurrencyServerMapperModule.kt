package mobdao.com.awesomecurrencyconverter.di.modules

import dagger.Module
import dagger.Provides
import dagger.Reusable
import mobdao.com.awesomecurrencyconverter.data.server.dataMappers.CurrencyServerMapper

@Module
@Suppress("unused")
object CurrencyServerMapperModule {
    @Provides
    @Reusable
    internal fun provideCurrencyServerMapper(): CurrencyServerMapper =
        CurrencyServerMapper
}