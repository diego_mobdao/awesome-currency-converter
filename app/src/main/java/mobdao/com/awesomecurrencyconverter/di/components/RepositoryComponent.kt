package mobdao.com.awesomecurrencyconverter.di.components

import dagger.Component
import mobdao.com.awesomecurrencyconverter.data.repositories.CurrencyRepository
import mobdao.com.awesomecurrencyconverter.di.modules.ContextModule
import mobdao.com.awesomecurrencyconverter.di.modules.CurrencyServerMapperModule
import mobdao.com.awesomecurrencyconverter.di.modules.CurrencyServiceModule
import javax.inject.Singleton

@Singleton
@Component(
    modules = [
        (ContextModule::class),
        (CurrencyServiceModule::class),
        (CurrencyServerMapperModule::class)
    ]
)
interface RepositoryComponent {
    fun inject(repository: CurrencyRepository)

    @Component.Builder
    interface Builder {
        fun build(): RepositoryComponent

        fun contextModule(contextModule: ContextModule): Builder
        fun currencyServiceModule(currencyServiceModule: CurrencyServiceModule): Builder
        fun currencyServerMapperModule(currencyServerMapperModule: CurrencyServerMapperModule): Builder
    }
}