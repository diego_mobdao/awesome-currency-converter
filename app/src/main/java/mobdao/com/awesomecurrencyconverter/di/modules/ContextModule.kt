package mobdao.com.awesomecurrencyconverter.di.modules

import android.app.Application
import android.content.Context
import dagger.Module
import dagger.Provides
import mobdao.com.awesomecurrencyconverter.modules.base.BaseContracts

@Module
object ContextModule {
    @Provides
    internal fun provideContext(
        baseView: BaseContracts.View
    ): Context {
        return baseView.getContext()
    }

    @Provides
    internal fun provideApplication(
        context: Context
    ): Application {
        return context.applicationContext as Application
    }
}