package mobdao.com.awesomecurrencyconverter.data.server.dataMappers

import mobdao.com.awesomecurrencyconverter.data.server.responses.CurrencyRatesResponse
import mobdao.com.awesomecurrencyconverter.models.CurrencyRate

object CurrencyServerMapper {

    fun currencyRatesResponseToCurrencyRateModel(
        currencyRatesResponse: CurrencyRatesResponse
    ): List<CurrencyRate> =
        with(currencyRatesResponse) {
            return rates?.toList()?.mapNotNull { pair ->
                getCurrencyRate(
                    code = pair.first,
                    rate = pair.second
                )
            }.orEmpty()
        }

    //region private

    private fun getCurrencyRate(
        code: String?,
        rate: Double?
    ): CurrencyRate? {
        if (code == null || rate == null) return null
        return CurrencyRate(
            code = code,
            rate = rate
        )
    }

    //endregion
}