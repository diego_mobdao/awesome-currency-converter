package mobdao.com.awesomecurrencyconverter.data.server.webServices

import io.reactivex.Single
import mobdao.com.awesomecurrencyconverter.data.server.responses.CurrencyRatesResponse
import retrofit2.http.GET
import retrofit2.http.Query

interface CurrencyService {

    @GET("latest")
    fun fetchComparedRates(
        @Query("base") baseCurrency: String
    ): Single<CurrencyRatesResponse>
}