package mobdao.com.awesomecurrencyconverter.data.db.typeconverters

import androidx.room.TypeConverter
import com.google.gson.Gson
import mobdao.com.awesomecurrencyconverter.utils.enums.CurrencyType

class CurrencyTypeConverter {

    @TypeConverter
    fun toCurrencyType(
        text: String
    ): CurrencyType? {
        if (text.isEmpty()) {
            return null
        }
        return try {
            Gson().fromJson(text, CurrencyType::class.java)
        } catch (exception: Exception) {
            null
        }
    }

    @TypeConverter
    fun toString(
        currencyType: CurrencyType?
    ): String {
        if (currencyType == null) {
            return ""
        }
        return try {
            Gson().toJson(currencyType)
        } catch (exception: Exception) {
            ""
        }
    }
}