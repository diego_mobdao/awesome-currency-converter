package mobdao.com.awesomecurrencyconverter.data.db

import android.app.Application
import androidx.room.Database
import androidx.room.Room
import androidx.room.RoomDatabase
import androidx.room.TypeConverters
import mobdao.com.awesomecurrencyconverter.data.db.typeconverters.CurrencyTypeConverter
import mobdao.com.awesomecurrencyconverter.data.db.daos.ConvertedCurrencyDao
import mobdao.com.awesomecurrencyconverter.models.entities.ConvertedCurrency
import mobdao.com.awesomecurrencyconverter.utils.constants.Constants.appDataBaseName

@Database(
    entities = [ConvertedCurrency::class],
    version = 1
)
@TypeConverters(value = [CurrencyTypeConverter::class])
abstract class AppDatabase : RoomDatabase() {

    abstract fun convertedCurrencyDao(): ConvertedCurrencyDao

    companion object {
        private var INSTANCE: AppDatabase? = null

        fun getInstance(
            application: Application
        ): AppDatabase {
            if (INSTANCE == null) {
                synchronized(AppDatabase::class) {
                    INSTANCE = Room.databaseBuilder(
                        application,
                        AppDatabase::class.java,
                        appDataBaseName
                    )
                        .fallbackToDestructiveMigration()
                        .build()
                }
            }
            return INSTANCE!! // will never be null
        }
    }
}