package mobdao.com.awesomecurrencyconverter.data.db.daos

import androidx.room.Dao
import androidx.room.Insert
import androidx.room.OnConflictStrategy
import androidx.room.Query
import io.reactivex.Completable
import io.reactivex.Observable
import mobdao.com.awesomecurrencyconverter.models.entities.ConvertedCurrency

@Dao
interface ConvertedCurrencyDao {

    @Query("SELECT * FROM converted_currency_table ORDER BY is_base DESC")
    fun getAll(): Observable<List<ConvertedCurrency>>

    @Insert(onConflict = OnConflictStrategy.REPLACE)
    fun insertAll(
        convertedCurrencies: List<ConvertedCurrency>
    ): Completable
}