package mobdao.com.awesomecurrencyconverter.data.repositories

import io.reactivex.Completable
import io.reactivex.Observable
import io.reactivex.Single
import mobdao.com.awesomecurrencyconverter.data.db.AppDatabase
import mobdao.com.awesomecurrencyconverter.data.db.daos.ConvertedCurrencyDao
import mobdao.com.awesomecurrencyconverter.data.server.dataMappers.CurrencyServerMapper
import mobdao.com.awesomecurrencyconverter.data.server.webServices.CurrencyService
import mobdao.com.awesomecurrencyconverter.di.components.DaggerRepositoryComponent
import mobdao.com.awesomecurrencyconverter.di.modules.ContextModule
import mobdao.com.awesomecurrencyconverter.di.modules.CurrencyServerMapperModule
import mobdao.com.awesomecurrencyconverter.di.modules.CurrencyServiceModule
import mobdao.com.awesomecurrencyconverter.models.CurrencyRate
import mobdao.com.awesomecurrencyconverter.models.entities.ConvertedCurrency
import javax.inject.Inject

class CurrencyRepository @Inject constructor(
    private val appDatabase: AppDatabase,
    private val convertedCurrencyDao: ConvertedCurrencyDao
) {

    @Inject
    lateinit var currencyService: CurrencyService

    @Inject
    lateinit var currencyServerMapper: CurrencyServerMapper

    init {
        DaggerRepositoryComponent
            .builder()
            .contextModule(ContextModule)
            .currencyServiceModule(CurrencyServiceModule)
            .currencyServerMapperModule(CurrencyServerMapperModule)
            .build()
            .inject(this)
    }

    fun fetchComparedRates(
        baseCurrency: String
    ): Single<List<CurrencyRate>> {
        return currencyService.fetchComparedRates(baseCurrency)
            .flatMap { response ->
                Single.just(
                    currencyServerMapper.currencyRatesResponseToCurrencyRateModel(
                        response
                    )
                )
            }
    }

    fun saveConvertedCurrencies(
        convertedCurrencies: List<ConvertedCurrency>
    ): Completable {
        return convertedCurrencyDao.insertAll(convertedCurrencies)
    }

    fun getSaveConvertedCurrencies(): Observable<List<ConvertedCurrency>> {
        return convertedCurrencyDao.getAll()
    }

    fun isInTransaction(): Boolean =
        appDatabase.inTransaction()
}