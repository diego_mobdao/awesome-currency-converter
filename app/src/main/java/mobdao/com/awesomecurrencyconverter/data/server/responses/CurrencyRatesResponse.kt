package mobdao.com.awesomecurrencyconverter.data.server.responses

data class CurrencyRatesResponse(
    var base: String? = null,
    var rates: HashMap<String, Double>? = null
)