package mobdao.com.awesomecurrencyconverter.models.entities

import androidx.room.ColumnInfo
import androidx.room.Entity
import androidx.room.PrimaryKey
import mobdao.com.awesomecurrencyconverter.utils.enums.CurrencyType

@Entity(
    tableName = "converted_currency_table"
)
class ConvertedCurrency(
    @PrimaryKey val type: CurrencyType,
    var rate: Double,
    @ColumnInfo(name = "is_base") var isBase: Boolean,
    @ColumnInfo(name = "converted_value") var convertedValue: Double
)