package mobdao.com.awesomecurrencyconverter.models

class CurrencyRate(
    val code: String,
    var rate: Double
)