# Awesome Currency Converter #

Awesome Currency Converter is an app written in Kotlin with MVP architecture, that converts a currency value to a huge list of other currencies.

![](arts/app_sample.png)

### Features ###

* Select a base currency to convert.
* Change base currency value.
* Get updated conversions in real-time.
* Data persistence so that the base currency value and the order of currencies is never lost
* When opening the app for the first time, the base currency is chosen based on device's Locale

### Third-party Libraries ###

* [Retrofit](https://square.github.io/retrofit)
* [Room](https://developer.android.com/topic/libraries/architecture/room)
* [RxKotlin](https://github.com/ReactiveX/RxKotlin)
* [Gson](https://github.com/google/gson)
* [HttpInterceptor](https://github.com/square/okhttp/tree/master/okhttp-logging-interceptor)
* [Dagger](https://github.com/google/dagger)

### Build ###

To build using Android Studio, just clone the repository and import on Android Studio.